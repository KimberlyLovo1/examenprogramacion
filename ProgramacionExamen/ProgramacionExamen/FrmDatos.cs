﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProgramacionExamen
{
    public partial class FrmDatos : Form
    {
        private int count;
        private DataSet dsDatos;
        private BindingSource bsDatos;
        public FrmDatos()
        {
            InitializeComponent();
        }
        public DataSet DsDatos
        {
            set
            {
                dsDatos = value;
            }
        }

        public BindingSource BsDatos
        {
            set
            {
                bsDatos = value;
            }
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (isNull())
            {
                MessageBox.Show("Rellenar datos", "Mensaje de error", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return;
            }

            double plazoTotal = Double.Parse(txtPlazo.Text) * 12;
            DateTime dt = dtpFecha.Value;
            double tazaPeriodoMensual = (Double.Parse(txtTasa.Text) / 100) / 12;

            double cuotaNumerador = Double.Parse(txtMonto.Text) * (tazaPeriodoMensual *
                Math.Pow((1 + tazaPeriodoMensual), plazoTotal));

            double cuotaMensual = cuotaNumerador /
                (Math.Pow((1 + tazaPeriodoMensual), plazoTotal) - 1);

            double saldo = Double.Parse(txtMonto.Text);

            int cont = 0;
            for (int i = 1; i <= plazoTotal; i++)
            {
                cont = cont + 30;
                double interes = saldo * tazaPeriodoMensual;
                double principal = cuotaMensual - interes;
                saldo -= principal;

                DataRow drDatos = dsDatos.Tables["Datos"].NewRow();
                drDatos["Id"] = i;
                drDatos["Cuota"] = cuotaMensual;
                drDatos["Principal"] = principal;
                drDatos["Pago"] = saldo;
                drDatos["Interes"] = interes;
                drDatos["Fecha"] = dt.AddDays(cont);

                dsDatos.Tables["Datos"].Rows.Add(drDatos);
                bsDatos.DataSource = dsDatos;
                bsDatos.DataMember = dsDatos.Tables["Datos"].TableName;

                dgvDatos.DataSource = bsDatos;
            }
        }

        private bool isNull()
        {
            if (txtTasa.Text == "" || txtMonto.Text == "" || txtPlazo.Text == "")
            {
                return true;
            }

            return false;
        }

        private void txtMonto_KeyPress(object sender, KeyPressEventArgs e)
        {
            numerosDecimales(e, txtMonto);
        }
        public void numerosDecimales(KeyPressEventArgs v, TextBox t)
        {
            if (char.IsDigit(v.KeyChar)) v.Handled = false;
            else if (char.IsSeparator(v.KeyChar)) v.Handled = false;
            else if (char.IsControl(v.KeyChar)) v.Handled = false;
            else if (v.KeyChar.ToString().Equals("."))
            {

                for (int i = 0; i < t.Text.Length; i++)
                {
                    if (t.Text.Substring(i, 1).Equals(".")) ++count;
                }

                count++;

                if (count > 1)
                {
                    v.Handled = true;
                    count = 0;
                }
                else v.Handled = false;
            }
            else v.Handled = true;
        }

        private void txtPlazo_KeyPress(object sender, KeyPressEventArgs e)
        {
            numerosDecimales(e, txtPlazo);
        }

        private void txtTasa_KeyPress(object sender, KeyPressEventArgs e)
        {
            numerosDecimales(e, txtTasa);
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void btnReport_Click(object sender, EventArgs e)
        {
            int rowCount = dgvDatos.Rows.Count; //valor del dgv
            if (rowCount <= 0)
            {
                MessageBox.Show(this, "Calcular primero", "Mensaje de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            ReportDato r = new ReportDato();

            for (int i = 0; i < dgvDatos.Rows.Count; i++)
            {
                Estructura dato = new Estructura();

                dato.NoCuota = (int)this.dgvDatos.Rows[i].Cells[0].Value;
                dato.Fecha = (DateTime)this.dgvDatos.Rows[i].Cells[1].Value;
                dato.Interes = (double)this.dgvDatos.Rows[i].Cells[2].Value;
                dato.Principal = (double)this.dgvDatos.Rows[i].Cells[3].Value;
                dato.Cuota = (double)this.dgvDatos.Rows[i].Cells[4].Value;
                dato.Pago = (double)this.dgvDatos.Rows[i].Cells[5].Value;
                r.datos.Add(dato);
            }
            r.ShowDialog();
        }
    }
}
