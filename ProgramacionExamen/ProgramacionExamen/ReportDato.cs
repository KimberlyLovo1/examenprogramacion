﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProgramacionExamen
{
    public partial class ReportDato : Form
    {
        public List<Estructura> datos = new List<Estructura>();
        public ReportDato()
        {
            InitializeComponent();
        }

        private void ReportDato_Load(object sender, EventArgs e)
        {
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "ProgramaExamen.ReportDato.rdlc";
            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DSdato", datos));

            this.reportViewer1.RefreshReport();
        }
    }
}
