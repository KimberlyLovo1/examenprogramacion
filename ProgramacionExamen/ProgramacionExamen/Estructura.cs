﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgramacionExamen
{
    public class Estructura
    {
        public int NoCuota { get; set; }
        public DateTime Fecha { get; set; }
        public double Interes { get; set; }
        public double Principal { get; set; }
        public double Cuota { get; set; }
        public double Pago { get; set; }

        public Estructura(int noCuota, DateTime fecha, double interes, double principal, double cuota, double pago)
        {
            NoCuota = noCuota;
            Fecha = fecha;
            Interes = interes;
            Principal = principal;
            Cuota = cuota;
            Pago = pago;
        }

        public Estructura()
        {
        }
    }
}
