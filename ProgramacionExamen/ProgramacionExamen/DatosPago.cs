﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProgramacionExamen
{
    public class DatosPago
    {
        private double años;
        private double monto;
        private double tasa;
        private DateTime fecha;

        public DatosPago()
        {
        }

        public DatosPago(double años, double monto, double tasa, DateTime fecha)
        {
            this.años = años;
            this.monto = monto;
            this.tasa = tasa;
            this.fecha = fecha;
        }

        public double Años { get => años; set => años = value; }
        public double Monto { get => monto; set => monto = value; }
        public double Tasa { get => tasa; set => tasa = value; }
        public DateTime Fecha { get => fecha; set => fecha = value; }

    }
}
